# UnicodeMathMod
Sublime package modification: Modifies Package Control - UnicodeMath to abbreviate and rename commonly used characters

Allows|instead of|to produce
---|---|---|
`"\grad"`|`"\nabla"`|∇|
`"\inf"`|`"\infty"`|∞|
`"\real"`|`"\BbbR"`|ℝ|
`"\null"`|`"\BbbN"`|ℕ|
`"\union"`|`"\cup"`|∪|
`"\intersection"`|`"\cap"`|∩|
`"\+-"`|`"\pm"`|±|
`"\phi2"`|`"\upvarphi"`|φ|
`"\def"`|`"\=="`|≡|
`"\ee"`, `"\ii"`, `"\jj"`, `"\kk"`, |`"\mscre"`, `"\mscri"`, `"\mscrj"`, `"\mscrk"`, |ℯ, 𝒾, 𝒿, 𝓀|
`"\..."`|`"\unicodeellipsis"`|…|
`"\rar"`, `"\lar"`, etc|`"\rightarrow"`, `"\leftarrow"`, etc|→, ←, etc|
`"\alp"`, `"\Alp"`, `"\bet"`, `"\Bet"`, etc|`"\alpha"`, `"\Alpha"`, `"\beta"`, `"\Beta"`, etc|α, Α, β, Β, etc|
`"\blockA"`, `"\blocka"`, `"\blockB"`, `"\blockb"`, etc|`"\BbbA"`, `"Bbba"`, `"\BbbB"`, `"Bbbb"`, etc|𝔸, 𝕒, 𝔹, 𝕓|
`"\scriptA"`, `"\scripta"`, `"\scriptB"`, `"\scriptb"`, etc|`"\mscrA"`, `"\mscra"`, `"\mscrB"`, `"\mscrb"`, etc|𝒜, 𝒶, ℬ, 𝒷|
`"\ubar"`|`"\underbar"`|̱|

After installing [Sublime3](https://www.sublimetext.com/3), [PackageControl](https://packagecontrol.io/), and [UnicodeMath](https://packagecontrol.io/packages/UnicodeMath), run Terminal and enter  
`git clone https://github.com/tllado/UnicodeMathMod`  
`./UnicodeMathMod/install.sh`  
