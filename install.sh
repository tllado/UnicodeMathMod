#!/bin/bash

if [ ! -f ~/Library/Application\ Support/Sublime\ Text\ 3/Installed\ Packages/UnicodeMath.sublime-package ]; then
    echo "Please install UnicodeMath via PackageControl, then run this script"
else
    mkdir ~/Library/Application\ Support/Sublime\ Text\ 3/Packages/UnicodeMath
    cp ./UnicodeMathMod/mathsymbols.py ~/Library/Application\ Support/Sublime\ Text\ 3/Packages/UnicodeMath/mathsymbols.py
    echo "All done. Have a nice day."
fi
